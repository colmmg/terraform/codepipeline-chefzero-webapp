#!/bin/bash -v

yum update -y
yum install -y ruby
cd /root
wget "https://aws-codedeploy-us-east-1.s3.amazonaws.com/latest/install"
chmod +x ./install
./install auto
service codedeploy-agent start
rm install
