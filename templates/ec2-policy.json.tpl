{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect":"Allow",
      "Resource":[
        "arn:aws:s3:::${codepipeline-artifacts-bucket}",
        "arn:aws:s3:::${codepipeline-artifacts-bucket}/*"
      ],
      "Action":[
        "s3:GetObject",
        "s3:GetObjectVersion",
        "s3:GetBucketAcl",
        "s3:GetBucketLocation"
      ]
    },
    {
      "Action": [
        "ec2:Describe*",
        "ec2:Get*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
