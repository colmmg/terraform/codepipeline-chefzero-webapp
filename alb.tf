resource "aws_lb" "webapp" {
  load_balancer_type = "application"
  name               = "${var.app}"
  security_groups    = [aws_security_group.lb.id]
  subnets = flatten([var.public_subnets])
  tags = {
    Name = "${var.app}"
  }
}

resource "aws_lb_listener" "webapp" {
  default_action {
    target_group_arn = aws_lb_target_group.webapp.arn
    type             = "forward"
  }
  load_balancer_arn = aws_lb.webapp.arn
  port              = "80"
  protocol          = "HTTP"
}

resource "aws_lb_target_group" "webapp" {
  deregistration_delay = "5"
  name                 = "${var.app}"
  port                 = 8080
  protocol             = "HTTP"
  vpc_id               = var.vpc_id
  health_check {
    healthy_threshold   = "2"
    interval            = "30"
    matcher             = "200"
    path                = "/"
    port                = "8080"
    protocol            = "HTTP"
    timeout             = "5"
    unhealthy_threshold = "2"
  }
  tags = {
    Name = "${var.app}"
  }
}
