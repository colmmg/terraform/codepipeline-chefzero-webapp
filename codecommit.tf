resource "aws_codecommit_repository" "webapp" {
  repository_name = "${var.app}"
  tags = {
    Name = "${var.app}"
  }
}
