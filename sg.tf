resource "aws_security_group" "lb" {
  description = "${var.app}-lb-sg"
  name        = "${var.app}-lb-sg"
  tags = {
    Name = "${var.app}-lb-sg"
  }
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "lb-http" {
  cidr_blocks       = ["${var.sg_cidr}"]
  description       = "HTTP access"
  from_port         = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.lb.id
  to_port           = 80
  type              = "ingress"
}

resource "aws_security_group_rule" "lb-http-tomcat-asg" {
  description              = "HTTP (tomcat) access to asg instance"
  from_port                = 8080
  protocol                 = "tcp"
  security_group_id        = aws_security_group.lb.id
  source_security_group_id = aws_security_group.asg.id
  to_port                  = 8080
  type                     = "egress"
}

resource "aws_security_group" "asg" {
  description = "${var.app}-asg-sg"
  name        = "${var.app}-asg-sg"
  tags = {
    Name = "${var.app}-asg-sg"
  }
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "asg-http-tomcat-lb" {
  description              = "HTTP (tomcat) access from ALB"
  from_port                = 8080
  protocol                 = "tcp"
  security_group_id        = aws_security_group.asg.id
  source_security_group_id = aws_security_group.lb.id
  to_port                  = 8080
  type                     = "ingress"
}

resource "aws_security_group_rule" "asg-outbound" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.asg.id
  to_port           = 0
  type              = "egress"
}
