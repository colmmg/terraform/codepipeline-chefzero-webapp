resource "aws_cloudwatch_log_group" "webapp" {
  name = "/aws/codebuild/${var.app}"
  tags = {
    Name = "/aws/codebuild/${var.app}"
  }
}
