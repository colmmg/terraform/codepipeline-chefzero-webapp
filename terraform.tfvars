app             = "codepipeline-chefzero-webapp"
private_subnets = ["subnet-abcd1234abcd12341", "subnet-abcd1234abcd12342"]
public_subnets  = ["subnet-abcd1234abcd12343", "subnet-abcd1234abcd12344"]
region          = "eu-west-1"
vpc_id          = "vpc-abcd1234abcd12345"
