variable "app" {
  description = "A string identifier to use to create unique resources."
}

variable "private_subnets" {
  description = "The subnets where the application's EC2 instances will be launched in." 
  type        = list(string)
}

variable "public_subnets" {
  description = "The subnets where the application load balancer nodes will be created in."
  type        = list(string)
}

variable "region" {
  description = "The AWS region."
}

variable "sg_cidr" {
  default     = "0.0.0.0/0"
  description = "The CIDR to set on the application load balancer security group allowing HTTP access."
}

variable "vpc_id" {
  description = "The id of the VPC to deploy the application to."
}
