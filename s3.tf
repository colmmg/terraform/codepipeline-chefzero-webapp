resource "aws_s3_bucket" "codebuild-artifacts" {
  acl           = "private"
  bucket_prefix = "codebuild-artifacts-"
  force_destroy = "true"
  lifecycle_rule {
    prefix  = ""
    enabled = true
    noncurrent_version_expiration {
      days = 365
    }
  }
  region = var.region
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
    }
  }
  tags = {
    Name = "codebuild-artifacts"
  }
  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket" "codepipeline-artifacts" {
  acl           = "private"
  bucket_prefix = "codepipeline-artifacts-"
  force_destroy = "true"
  lifecycle_rule {
    prefix  = ""
    enabled = true
    noncurrent_version_expiration {
      days = 365
    }
  }
  region = var.region
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
    }
  }
  tags = {
    Name = "codepipeline-artifacts"
  }
  versioning {
    enabled = true
  }
}
