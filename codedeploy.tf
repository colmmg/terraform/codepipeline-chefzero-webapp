data "aws_iam_policy" "codedeploy" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
}

data "aws_iam_policy_document" "codedeploy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codedeploy.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codedeploy" {
  assume_role_policy = data.aws_iam_policy_document.codedeploy.json
  name               = "${var.app}-codedeploy"
  tags = {
    Name = "${var.app}-codedeploy"
  }
}

resource "aws_iam_role_policy_attachment" "codedeploy" {
  policy_arn = data.aws_iam_policy.codedeploy.arn
  role       = aws_iam_role.codedeploy.name
}

data "template_file" "codedeploy" {
  template = file("${path.module}/templates/codedeploy-policy.json.tpl")
}

resource "aws_iam_role_policy" "codedeploy" {
  name   = "${var.app}-codedeploy"
  policy = data.template_file.codedeploy.rendered
  role   = aws_iam_role.codedeploy.id
}

resource "aws_codedeploy_app" "webapp" {
  compute_platform = "Server"
  name             = var.app
}

resource "aws_codedeploy_deployment_group" "webapp" {
  app_name               = aws_codedeploy_app.webapp.name
  deployment_config_name = "CodeDeployDefault.AllAtOnce"
  deployment_group_name  = var.app
  service_role_arn       = aws_iam_role.codedeploy.arn
 
  autoscaling_groups     = ["${aws_autoscaling_group.webapp.name}"]

  deployment_style {
    deployment_option = "WITH_TRAFFIC_CONTROL"
    deployment_type   = "BLUE_GREEN"
  }

  blue_green_deployment_config {
    deployment_ready_option {
      action_on_timeout    = "CONTINUE_DEPLOYMENT"
      wait_time_in_minutes = 0
    }

    green_fleet_provisioning_option {
      action = "COPY_AUTO_SCALING_GROUP"
    }

    terminate_blue_instances_on_deployment_success {
      action                           = "TERMINATE"
      termination_wait_time_in_minutes = 0
    }
  }

  auto_rollback_configuration {
    enabled = true
    events  = ["DEPLOYMENT_FAILURE"]
  }

  load_balancer_info {
    target_group_info {
      name = "${var.app}"
    }
  }
}
