data "aws_iam_policy_document" "ec2" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ec2" {
  assume_role_policy = data.aws_iam_policy_document.ec2.json
  name               = "${var.app}-ec2"
  tags = {
    Name = "${var.app}-ec2"
  }
}

data "template_file" "ec2" {
  template = file("${path.module}/templates/ec2-policy.json.tpl")
  vars = {
    codepipeline-artifacts-bucket = aws_s3_bucket.codepipeline-artifacts.id
  }
}

resource "aws_iam_role_policy" "ec2" {
  name   = "${var.app}-ec2"
  policy = data.template_file.ec2.rendered
  role   = aws_iam_role.ec2.id
}

resource "aws_iam_instance_profile" "ec2" {
  name  = "${var.app}-ec2"
  role  = aws_iam_role.ec2.name
}

data "aws_ami" "amazonlinux" {
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  most_recent = true
  owners      = ["137112412989"]
}

data "template_file" "user-data" {
  template = file("${path.module}/templates/user_data.tpl")
}

resource "aws_launch_template" "webapp" {
  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      delete_on_termination = "true"
      volume_size           = "10"
      volume_type           = "gp2"
    }
  }
  iam_instance_profile {
    name = "${aws_iam_instance_profile.ec2.name}"
  }
  image_id      = data.aws_ami.amazonlinux.id
  instance_type = "t3.micro"
  name = "${var.app}"
  tags = {
    Name = "${var.app}"
  }
  user_data              = base64encode(data.template_file.user-data.rendered)
  vpc_security_group_ids = [aws_security_group.asg.id]
}

resource "aws_autoscaling_group" "webapp" {
  name                      = "${var.app}"
  min_size                  = "1"
  max_size                  = "1"
  health_check_grace_period = "180"
  health_check_type         = "ELB"
  force_delete              = false
  launch_template {
    id      = aws_launch_template.webapp.id
    version = "$Latest"
  }
  target_group_arns = [aws_lb_target_group.webapp.arn]
  min_elb_capacity  = "1"
  suspended_processes = ["AlarmNotification", "HealthCheck"]
  tag {
    key                 = "Name"
    value               = "${var.app}"
    propagate_at_launch = true
  }
  vpc_zone_identifier = flatten([var.private_subnets])
  wait_for_capacity_timeout = "0"
}

