data "aws_iam_policy_document" "codepipeline" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codepipeline.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codepipeline" {
  assume_role_policy = data.aws_iam_policy_document.codepipeline.json
  name               = "${var.app}-codepipeline"
  path               = "/service-role/"
  tags = {
    Name = "${var.app}-codepipeline"
  }
}

data "template_file" "codepipeline" {
  template = file("${path.module}/templates/codepipeline-policy.json.tpl")
}

resource "aws_iam_role_policy" "codepipeline" {
  name   = "${var.app}-codepipeline"
  policy = data.template_file.codepipeline.rendered
  role   = aws_iam_role.codepipeline.id
}
