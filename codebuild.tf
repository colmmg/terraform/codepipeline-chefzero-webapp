data "aws_iam_policy_document" "codebuild" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codebuild" {
  assume_role_policy = data.aws_iam_policy_document.codebuild.json
  name               = "${var.app}-codebuild"
  path               = "/service-role/"
  tags = {
    Name = "${var.app}-codebuild"
  }
}

data "template_file" "codebuild" {
  template = file("${path.module}/templates/codebuild-policy.json.tpl")
  vars = {
    app                           = var.app
    aws_account_id                = data.aws_caller_identity.current.account_id
    codebuild-artifacts-bucket    = aws_s3_bucket.codebuild-artifacts.id
    codepipeline-artifacts-bucket = aws_s3_bucket.codepipeline-artifacts.id
    region                        = var.region
  }
}

resource "aws_iam_role_policy" "codebuild" {
  name   = "${var.app}-codebuild"
  policy = data.template_file.codebuild.rendered
  role   = aws_iam_role.codebuild.id
}

resource "aws_codebuild_project" "webapp" {
  artifacts {
    type = "S3"
    location = "${aws_s3_bucket.codebuild-artifacts.id}"
    name = "artifacts"
    namespace_type = "BUILD_ID"
    packaging = "NONE"
    path = "/${var.app}/"
  }
  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/amazonlinux2-x86_64-standard:2.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
  }
  logs_config {
    cloudwatch_logs {
      group_name = "/aws/codebuild/${var.app}"
    }
  }
  name          = var.app
  service_role  = aws_iam_role.codebuild.arn
  source {
    type            = "CODECOMMIT"
    location        = "${aws_codecommit_repository.webapp.clone_url_http}"
    git_clone_depth = 1
  }
  tags = {
    Name = "${var.app}"
  }
}
