# codepipeline-chefzero-webapp
This repository contains [Terraform](https://www.terraform.io/) code used to deploy prerequisite resources for use in a sample [AWS CodePipeline](https://aws.amazon.com/codepipeline/) web application.

This repository should be used together with [colmmg/chef/codepipeline-chefzero-webapp](https://gitlab.com/colmmg/chef/codepipeline-chefzero-webapp).

# Terraform Deploy Instructions
First, edit the variables in `vars.tf`. You should at the least, replace the subnet ids and vpc id with your own values.

To deploy run:
```
terraform init
terraform apply
```
